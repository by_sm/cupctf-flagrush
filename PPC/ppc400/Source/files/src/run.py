import socket as sock
import multiprocessing
import random
import sys
import subprocess
from main import s


PORT = 5003
ROUNDS = 1000
FLAG = "someflag"

def handler(connection):
    try:
        s()
    except KeyboardInterrupt:
        print('interrupt internal')
        connection.close()
        sys.exit(0)

socket = sock.socket(sock.AF_INET, sock.SOCK_STREAM)
socket.bind(('0.0.0.0', PORT))
socket.listen(1)

clients = []

while True:
    try:
        conn, address = socket.accept()

        clients.append(conn)

        print('connected:' + address[0])
        process = multiprocessing.Process(target=handler, args=(conn,))
        process.daemon = True
        process.start()
    except KeyboardInterrupt:

        for client in clients:
            client.close()

        socket.shutdown(sock.SHUT_RDWR)
        socket.close()

        break
