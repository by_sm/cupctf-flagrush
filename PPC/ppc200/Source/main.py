import socket as sock
import multiprocessing
import random
import sys

PORT = 5000
ROUNDS = 10
TARGET_VALUE = 4096
FLAG = "CTFCUP{random_u_and_r}"


myArray = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
inducerlist = [0, 1, 2, 3]
myArray[random.choice(inducerlist)][random.choice(inducerlist)] = 2

def show_matrix(myArray):
    matrix = "\n\n".join(["\t".join([str(myArray[i][j]) for j in range(4)]) for i in range(4)])
    return matrix


def users_choice(myArray, user_input):
    if user_input == "u":
        i = 0
        for j in range(0, 4):
            if myArray[i][j] != 0 or myArray[i + 1][j] != 0 or myArray[i + 2][j] != 0 or myArray[i + 3][j] != 0:
                if myArray[i][j] == 0:
                    while myArray[i][j] == 0:
                        myArray[i][j] = myArray[i + 1][j]
                        myArray[i + 1][j] = myArray[i + 2][j]
                        myArray[i + 2][j] = myArray[i + 3][j]
                        myArray[i + 3][j] = 0
                if myArray[i + 1][j] == 0 and (myArray[i + 2][j] != 0 or myArray[i + 3][j] != 0):
                    while myArray[i + 1][j] == 0:
                        myArray[i + 1][j] = myArray[i + 2][j]
                        myArray[i + 2][j] = myArray[i + 3][j]
                        myArray[i + 3][j] = 0
                if myArray[i + 2][j] == 0 and (myArray[i + 3][j] != 0):
                    while myArray[i + 2][j] == 0:
                        myArray[i + 2][j] = myArray[i + 3][j]
                        myArray[i + 3][j] = 0
        i = 0
        for j in range(0, 4):
            if myArray[i][j] == myArray[i + 1][j]:
                myArray[i][j] = myArray[i][j] + myArray[i + 1][j]
                myArray[i + 1][j] = myArray[i + 2][j]
                myArray[i + 2][j] = myArray[i + 3][j]
                myArray[i + 3][j] = 0
            if myArray[i + 1][j] == myArray[i + 2][j]:
                myArray[i + 1][j] = myArray[i + 1][j] + myArray[i + 2][j]
                myArray[i + 2][j] = myArray[i + 3][j]
                myArray[i + 3][j] = 0
            if myArray[i + 2][j] == myArray[i + 3][j]:
                myArray[i + 2][j] = myArray[i + 2][j] + myArray[i + 3][j]
                myArray[i + 3][j] = 0



    elif user_input == "d":
        i = 0
        for j in range(0, 4):
            if myArray[i][j] != 0 or myArray[i + 1][j] != 0 or myArray[i + 2][j] != 0 or myArray[i + 3][j] != 0:
                if myArray[i + 3][j] == 0:
                    while myArray[i + 3][j] == 0:
                        myArray[i + 3][j] = myArray[i + 2][j]
                        myArray[i + 2][j] = myArray[i + 1][j]
                        myArray[i + 1][j] = myArray[i][j]
                        myArray[i][j] = 0
                if myArray[i + 2][j] == 0 and (myArray[i + 1][j] != 0 or myArray[i][j] != 0):
                    while myArray[i + 2][j] == 0:
                        myArray[i + 2][j] = myArray[i + 1][j]
                        myArray[i + 1][j] = myArray[i][j]
                        myArray[i][j] = 0

                if myArray[i + 1][j] == 0 and myArray[i][j] != 0:
                    while myArray[i + 1][j] == 0:
                        myArray[i + 1][j] = myArray[i][j]
                        myArray[i][j] = 0
        i = 0
        for j in range(0, 4):
            if myArray[i + 3][j] == myArray[i + 2][j]:
                myArray[i + 3][j] = myArray[i + 3][j] + myArray[i + 2][j]
                myArray[i + 2][j] = myArray[i + 1][j]
                myArray[i + 1][j] = myArray[i][j]
                myArray[i][j] = 0
            if myArray[i + 2][j] == myArray[i + 1][j]:
                myArray[i + 2][j] = myArray[i + 2][j] + myArray[i + 1][j]
                myArray[i + 1][j] = myArray[i][j]
                myArray[i][j] = 0
            if myArray[i + 1][j] == myArray[i][j]:
                myArray[i + 1][j] = myArray[i + 1][j] + myArray[i][j]
                myArray[i][j] = 0

    elif user_input == "l":
        j = 0
        for i in range(0, 4):

            if myArray[i][j] != 0 or myArray[i][j + 1] != 0 or myArray[i][j + 2] != 0 or myArray[i][j + 3] != 0:
                if myArray[i][j] == 0:
                    while myArray[i][j] == 0:
                        myArray[i][j] = myArray[i][j + 1]
                        myArray[i][j + 1] = myArray[i][j + 2]
                        myArray[i][j + 2] = myArray[i][j + 3]
                        myArray[i][j + 3] = 0
                if myArray[i][j + 1] == 0 and (myArray[i][j + 2] != 0 or myArray[i][j + 3] != 0):
                    while myArray[i][j + 1] == 0:
                        myArray[i][j + 1] = myArray[i][j + 2]
                        myArray[i][j + 2] = myArray[i][j + 3]
                        myArray[i][j + 3] = 0
                if myArray[i][j + 2] == 0 and (myArray[i][j + 3] != 0):
                    while myArray[i][j + 2] == 0:
                        myArray[i][j + 2] = myArray[i][j + 3]
                        myArray[i][j + 3] = 0
        j = 0
        for i in range(0, 4):
            if myArray[i][j] == myArray[i][j + 1]:
                myArray[i][j] = myArray[i][j] + myArray[i][j + 1]
                myArray[i][j + 1] = myArray[i][j + 2]
                myArray[i][j + 2] = myArray[i][j + 3]
                myArray[i][j + 3] = 0
            if myArray[i][j + 1] == myArray[i][j + 2]:
                myArray[i][j + 1] = myArray[i][j + 1] + myArray[i][j + 2]
                myArray[i][j + 2] = myArray[i][j + 3]
                myArray[i][j + 3] = 0
            if myArray[i][j + 2] == myArray[i][j + 3]:
                myArray[i][j + 2] = myArray[i][j + 2] + myArray[i][j + 3]
                myArray[i][j + 3] = 0
    elif user_input == "r":
        j = 0
        for i in range(0, 4):
            if myArray[i][j] != 0 or myArray[i][j + 1] != 0 or myArray[i][j + 2] != 0 or myArray[i][j + 3] != 0:
                if myArray[i][j + 3] == 0:
                    while myArray[i][j + 3] == 0:
                        myArray[i][j + 3] = myArray[i][j + 2]
                        myArray[i][j + 2] = myArray[i][j + 1]
                        myArray[i][j + 1] = myArray[i][j]
                        myArray[i][j] = 0
                if myArray[i][j + 2] == 0 and (myArray[i][j + 1] != 0 or myArray[i][j] != 0):
                    while myArray[i][j + 2] == 0:
                        myArray[i][j + 2] = myArray[i][j + 1]
                        myArray[i][j + 1] = myArray[i][j]
                        myArray[i][j] = 0

                if myArray[i][j + 1] == 0 and myArray[i][j] != 0:
                    while myArray[i][j + 1] == 0:
                        myArray[i][j + 1] = myArray[i][j]
                        myArray[i][j] = 0
        j = 0
        for i in range(0, 4):
            if myArray[i][j + 3] == myArray[i][j + 2]:
                myArray[i][j + 3] = myArray[i][j + 3] + myArray[i][j + 2]
                myArray[i][j + 2] = myArray[i][j + 1]
                myArray[i][j + 1] = myArray[i][j]
                myArray[i][j] = 0
            if myArray[i][j + 2] == myArray[i][j + 1]:
                myArray[i][j + 2] = myArray[i][j + 2] + myArray[i][j + 1]
                myArray[i][j + 1] = myArray[i][j]
                myArray[i][j] = 0
            if myArray[i][j + 1] == myArray[i][j]:
                myArray[i][j + 1] = myArray[i][j + 1] + myArray[i][j]
                myArray[i][j] = 0


def check_state(myArray):
    for i in range(4):
        for j in range(4):
            if myArray[i][j] == TARGET_VALUE:
                return True


def handler(connection):
    correct = 0
    try:
        for i in range(ROUNDS):
            if i != 0:
                connection.send("New game! \n".encode())

            myArray = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
            inducerlist = [0, 1, 2, 3]
            myArray[random.choice(inducerlist)][random.choice(inducerlist)] = 2

            while True:
                connection.send(show_matrix(myArray).encode())
                connection.send('\nu - up,\nd - down,\nl - left,\nr - right\n'.encode())

                data = connection.recv(1024)
                user_input = data.decode().split('\n')[0]
                print(user_input)

                users_choice(myArray, user_input)
                if check_state(myArray):
                    correct += 1
                    connection.send("Congratulation! \n".encode())
                    break

                listfori = []
                listforj = []
                count = 0
                for i in range(0, 4):
                    for j in range(0, 4):
                        if myArray[i][j] == 0:
                            count += 1
                            listfori.append(i)
                            listforj.append(j)
                if count > 0:
                    if count > 1:
                        randomindex = listfori.index(random.choice(listfori))
                        myArray[listfori[randomindex]][listforj[randomindex]] = 2
                    else:
                        myArray[listfori[0]][listforj[0]] = 2
                else:
                    continue

        if correct == ROUNDS:
            connection.send("Congratulation! Your flag - {} \n".format(FLAG).encode())

    except KeyboardInterrupt:
        print('interrupt internal')
        connection.close()
        sys.exit(0)

socket = sock.socket(sock.AF_INET, sock.SOCK_STREAM)
socket.bind(('0.0.0.0', PORT))
socket.listen(1)

clients = []

while True:
    try:
        conn, address = socket.accept()

        clients.append(conn)

        print('connected:' + address[0])
        process = multiprocessing.Process(target=handler, args=(conn,))
        process.daemon = True
        process.start()
    except KeyboardInterrupt:

        for client in clients:
            client.close()

        socket.shutdown(sock.SHUT_RDWR)
        socket.close()

        break

