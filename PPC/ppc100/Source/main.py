import socket as sock
import multiprocessing
import random
import sys

PORT = 5000
ROUNDS = 1000
FLAG = "CTFCUP{two_plus_two_is_so_simple}"

OPERATIONS = ['+', '-', '*']

SYMBOLS = {'1': [' ____       ', '/_   |      ', ' |   |      ', ' |   |      ', ' |___|      ', '            '], '7': ['_________   ', '\\______  \\  ', '    /    /  ', '   /    /   ', '  /____/    ', '            '], '/': ['     /\\     ', '    / /     ', '   / /      ', '  / /       ', ' / /        ', ' \\/         '], '-': ['            ', '            ', '  ______    ', ' /_____/    ', '            ', '            '], '+': ['            ', '    .__     ', '  __|  |___ ', ' /__    __/ ', '    |__|    ', '            '], '*': ['            ', '  /\\|\\/\\    ', ' _)    (__  ', ' \\_     _/  ', '   )    \\   ', '   \\/\\|\\/   '], '9': [' ________   ', '/   __   \\  ', '\\____    /  ', '   /    /   ', '  /____/    ', '            '], '4': ['   _____    ', '  /  |  |   ', ' /   |  |_  ', '/    ^   /  ', '\\____   |   ', '     |__|   '], '2': ['________    ', '\\_____  \\   ', ' /  ____/   ', '/       \\   ', '\\_______ \\  ', '        \\/  '], '3': ['________    ', '\\_____  \\   ', '  _(__  <   ', ' /       \\  ', '/______  /  ', '       \\/   '], '0': ['_______     ', '\\   _  \\    ', '/  /_\\  \\   ', '\\  \\_/   \\  ', ' \\_____  /  ', '       \\/   '], '5': [' .________  ', ' |   ____/  ', ' |____  \\   ', ' /       \\  ', '/______  /  ', '       \\/   '], '6': ['  ________  ', ' /  _____/  ', '/   __  \\   ', '\\  |__\\  \\  ', ' \\_____  /  ', '       \\/   '], '8': ['  ______    ', ' /  __  \\   ', ' >      <   ', '/   --   \\  ', '\\______  /  ', '       \\/   ']}


def generate_symbol(string):
    val = ['', '', '', '', '', '']

    for symbol in string:
        i = 0
        for line in SYMBOLS[symbol]:
            val[i] += SYMBOLS[symbol][i]
            i = i + 1

    return '\n'.join(val) + '\n'


def handler(connection):
    try:
        for i in range(ROUNDS):
            a = random.randint(1, 9999)
            b = random.randint(1, 9999)

            operation = OPERATIONS[random.randint(0, len(OPERATIONS) - 1)]

            if b > a:
                b, a = a, b

            s = '{}{}{}'.format(a, operation, b)

            answer = str(eval(s))

            connection.send(generate_symbol(s).encode())

            data = connection.recv(1024)
            client_answer = data.decode().split('\n')[0]

            if answer != client_answer:
                connection.send('Answer is incorrect. Bye!'.encode())
                connection.shutdown(sock.SHUT_RDWR)
                connection.close()
                return

        connection.send("Congratulations!\n You flag - {}".format(FLAG).encode())

    except KeyboardInterrupt:
        print('interrupt internal')
        connection.close()
        sys.exit(0)

socket = sock.socket(sock.AF_INET, sock.SOCK_STREAM)
socket.bind(('0.0.0.0', PORT))
socket.listen(1)

clients = []

while True:
    try:
        conn, address = socket.accept()

        clients.append(conn)

        print('connected:' + address[0])
        process = multiprocessing.Process(target=handler, args=(conn,))
        process.daemon = True
        process.start()
    except KeyboardInterrupt:

        for client in clients:
            client.close()

        socket.shutdown(sock.SHUT_RDWR)
        socket.close()

        break
